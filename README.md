# JIRA prepare-commit-msg hook

This [prepare-commit-msg](https://www.kernel.org/pub/software/scm/git/docs/githooks.html#_prepare_commit_msg) hook will automatically prepend the JIRA issue key prefix from the currently checked out branch to commit messages.

Your branch must start with an issue key (e.g. `STASH-1234-fix-a-bug`) for this to work. If your current branch _does not_ start with an issue key, the hook will leave the default commit message intact.

## Installation

From the root of your git repository, run the following:
    
    curl -so .git/hooks/prepare-commit-msg https://bitbucket.org/tpettersen/prepare-commit-msg-jira/raw/master/prepare-commit-msg
    chmod a+x .git/hooks/prepare-commit-msg

And that's it! Next time you commit, your message should have your current branch's issue key prefix prepended to it.
